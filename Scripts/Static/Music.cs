namespace Template;

public static class Music
{

	public static AudioStream Menu { get; } = 
		Load("birds.mp3");

	public static AudioStream Level1 { get; } =
		Load("space.mp3");

	static AudioStream Load(string path) =>
		GD.Load<AudioStream>($"res://Audio/Songs/{path}");
}
