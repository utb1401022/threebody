namespace Template;

public partial class UIMainMenuNav : Node
{
	public override void _Ready()
	{
	}

	void _on_play_pressed()
	{
		Global.Services.Get<AudioManager>().PlayMusic(Music.Level1, false);
		
		Global.Services.Get<SceneManager>().SwitchScene("space", 
			SceneManager.TransType.Fade);
	}
	
	void _on_player_control_pressed()
	{
		Global.Services.Get<AudioManager>().PlayMusic(Music.Level1, false);
		
		Global.Services.Get<SceneManager>().SwitchScene("player_control", 
			SceneManager.TransType.Fade);
	}

	void _on_options_pressed()
	{
		Global.Services.Get<SceneManager>().SwitchScene("Prefabs/UI/options");
	}

	void _on_quit_pressed() => 
		GetNode<Global>("/root/Global").Quit();
}


