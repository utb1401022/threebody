using Godot;
using System;
using System.Linq;
using System.Collections;

namespace Template;

public partial class Space : Node2D
{
	private Body redBody;
	private Body greenBody;
	private Body blueBody;
	private readonly int g = 10;
	private Vector2 screenSize;
	
	public override void _Ready()
	{
		var generator = new Random();
		int randomFactor = 200;
		screenSize = GetViewport().GetVisibleRect().Size;
		
		redBody = new Body(new Vector2(screenSize.X / 5 + generator.Next(randomFactor), screenSize.Y / 3 + generator.Next(randomFactor)), 5);
		greenBody = new Body(new Vector2(screenSize.X / 2 + generator.Next(randomFactor), screenSize.Y / 5 + generator.Next(randomFactor)), 5);
		blueBody = new Body(new Vector2(screenSize.X / 3 + generator.Next(randomFactor), screenSize.Y / 2 + generator.Next(randomFactor)), 5);
	}
	
	public override void _Draw()
	{
		DrawCircle(redBody.Position, redBody.Mass, Colors.Red);
		DrawCircle(greenBody.Position, greenBody.Mass, Colors.Green);
		DrawCircle(blueBody.Position, blueBody.Mass, Colors.Blue);
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	
		if(redBody.IsDead(new[] { greenBody, blueBody }, screenSize) ||
		   greenBody.IsDead(new[] { redBody, blueBody }, screenSize) ||
		   blueBody.IsDead(new[] { greenBody, redBody }, screenSize))
		{
			Global.Services.Get<SceneManager>().SwitchScene("main_menu", 
			SceneManager.TransType.Fade);
		}
		

		if (Input.IsKeyPressed(Key.Kp1))
		{
			redBody.Mass++;
		}
		if (Input.IsKeyPressed(Key.Kp2))
		{
			greenBody.Mass++;
		}
		if (Input.IsKeyPressed(Key.Kp3))
		{
			blueBody.Mass++;
		}
		
		float fDelta = (float)delta;
		redBody.UpdateVelocity(new [] { greenBody, blueBody }, fDelta, g);
		greenBody.UpdateVelocity(new[] { redBody, blueBody }, fDelta, g);
		blueBody.UpdateVelocity(new[] { greenBody, redBody }, fDelta, g);

		greenBody.UpdatePosition(fDelta);
		redBody.UpdatePosition(fDelta);
		blueBody.UpdatePosition(fDelta);

		QueueRedraw();
	}
}

