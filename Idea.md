﻿## Three body problem

Pro den otevřených dveří jsem chtěl udělat něco jednoduchého, ale hravého. Problém tří těles mě vždy fascinoval, takže premisa byla jasná.

### Version 1 - mass control

Nápad byl udělat simulaci tří těles, kde hráč musí předejít kolizím. K dispozici má ale jen jeden nástroj - zvyšování hmotnosti/velikosti jednotlivých těles. 

Bohužel, hratelnost byla frustrující a výsledek dost záležel na náhodě vstupních patametrů.


### Version 2 - player control

V této verzi hráč šipkami ovládá jedno (největší) ze tří těles, a musí systém udržet stabilní po co nejdelší dobu. To už se hraje příjemně, i když nápady na další variace jsou:

* N-body player control
* Increase gravity by time
* Two types of auto-generated bodies - to eat and to avoid
* etc...