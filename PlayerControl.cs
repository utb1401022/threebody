using Godot;
using System;
using System.Linq;
using System.Collections;

namespace Template;

public partial class PlayerControl : Node2D
{
	private Body redBody;
	private Body greenBody;
	private Body blueBody;
	private int gForce;
	private Vector2 screenSize;
	private float elapsedTime;
	
	public override void _Ready()
	{
		gForce = 8;
		elapsedTime = 0.0f;
		screenSize = GetViewport().GetVisibleRect().Size;
		int randomFactor = 50;
		var generator = new Random();

		redBody = new Body(new Vector2(screenSize.X / 2, screenSize.Y / 2), 20);
		greenBody = new Body(new Vector2(screenSize.X / 4 + generator.Next(randomFactor), screenSize.Y / 5 + generator.Next(randomFactor)), 10);
		blueBody = new Body(new Vector2(screenSize.X / 2 + generator.Next(randomFactor), screenSize.Y - generator.Next(randomFactor)), 10);
	}
	
	public override void _Draw()
	{
		DrawCircle(redBody.Position, redBody.Mass, Colors.Red);
		DrawCircle(greenBody.Position, greenBody.Mass, Colors.Green);
		DrawCircle(blueBody.Position, blueBody.Mass, Colors.Blue);
		
		DrawString(ThemeDB.FallbackFont, new Vector2(screenSize.X / 2 - 100, 30), $"Elapsed: {elapsedTime:F2}",
			   HorizontalAlignment.Center, 200, 25);
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		
		if(redBody.IsDead(new[] { greenBody, blueBody }, screenSize) ||
		   greenBody.IsDead(new[] { redBody }, screenSize) ||
		   blueBody.IsDead(new[] { redBody }, screenSize))
		{
			Global.TrySetTopScore(elapsedTime);
			Global.Services.Get<SceneManager>().SwitchScene("main_menu", 
			SceneManager.TransType.Fade);
		}

		var velocity = redBody.Velocity;
		int speed = 5;
		if (Input.IsKeyPressed(Key.Up))
		{
			velocity.Y -= speed;
		}
		if (Input.IsKeyPressed(Key.Down))
		{
			velocity.Y += speed;
		}
		if (Input.IsKeyPressed(Key.Right))
		{
			velocity.X += speed;
		}
		if (Input.IsKeyPressed(Key.Left))
		{
			velocity.X -= speed;
		}
		redBody.Velocity = velocity;

		float fDelta = (float)delta;
		elapsedTime += fDelta;
		greenBody.UpdateVelocity(new[] { redBody, blueBody }, fDelta, gForce);
		blueBody.UpdateVelocity(new[] { greenBody, redBody }, fDelta, gForce);

		greenBody.UpdatePosition(fDelta);
		redBody.UpdatePosition(fDelta);
		blueBody.UpdatePosition(fDelta);

		QueueRedraw();
	}
}

public class Body
{
	public Body(Vector2 position, int mass)
	{
		Position = position;
		Velocity = new Vector2(0, 0);
		Mass = mass;
	}
	
	public int Mass { get; set; }
	public Vector2 Position { get; set; }
	public Vector2 Velocity { get; set; }

	public bool IsDead(Body[] bodies, Vector2 size)
	{
		if(Position.X < 0 || Position.Y < 0 || Position.X > size.X || Position.Y > size.Y)
		{
			return true;
		}
		foreach (var body in bodies)
		{
			if (IsCollidingWith(body))
			{
				return true;
			}
		}
		return false;
	}

	public void UpdateVelocity(Body[] bodies, float delta, int g)
	{
		foreach (Body body in bodies) 
		{
			UpdateVelocity(body, delta, g);
		}
	}

	public void UpdatePosition(float delta)
	{
		Position += Velocity * delta;
	}

	private void UpdateVelocity(Body body, float delta, int g)
	{
		var direction = GetDirectionFrom(body).Normalized();
		float distance = direction.Length();
		float gForce = g * (Mass * body.Mass) / (distance * distance);
		Vector2 force = direction * gForce;
		Velocity -= force / Mass * delta;
	}

	private bool IsCollidingWith(Body body)
	{
		var distance = GetDirectionFrom(body).Length();
		return distance < (Mass + body.Mass);
	}

	private Vector2 GetDirectionFrom(Body body)
	{
		return (Position - body.Position);
	}
}

